import pandas as pd
import sqlalchemy as sa  # type: ignore
from ..classes import Base, GalaxySim
from ..helpers import glade_filter


def read_in_catalog(filename="catalogs/GLADE+_reduced.txt"):
    columns = [
        "glade",
        "hyperleda",
        "wise",
        "quasar_cluster_flag",
        "ra",
        "dec",
        "m_B",
        "Bflag",
        "m_K",
        "m_W1",
        "W1_flag",
        "redshift_cmb",
        "redshift_corrected_pec_vel",
        "error_peculiar_velocity",
        "redshift_lum_distance_flag",
    ]

    glade_chunk = pd.read_csv(filename, chunksize=5000, names=columns, sep=r"\s+")
    return glade_chunk

def read_in_upglade(filename):
    columns = [
        "id",
        "DR10",
        "DR9_SGA",
        "DR8",
        "WISE",
        "PS_objid",
        "PS_unique_id",
        "SDSS_bestobjid",
        "SkyMapper",
        "GLADEp",
        "ra",
        "dec",
        "ra_err",
        "dec_err",
        "g",
        "g_err",
        "g_kcorr",
        "g_dkdz",
        "r",
        "r_err",
        "r_kcorr",
        "r_dkdz",
        "i",
        "i_err",
        "i_kcorr",
        "i_dkdz",
        "z",
        "z_err",
        "z_kcorr",
        "z_dkdz",
        "y",
        "y_err",
        "y_kcorr",
        "y_dkdz",
        "w1",
        "w1_err",
        "w1_kcorr",
        "w1_dkdz",
        "w2",
        "w2_err",
        "w2_kcorr",
        "w2_dkdz",
        "w3",
        "w3_err",
        "w3_kcorr",
        "w3_dkdz",
        "w4",
        "w4_err",
        "w4_kcorr",
        "w4_dkdz",
        "zphot",
        "zphot_err",
        "zphot_median",
        "zphot_l68",
        "zphot_u68",
        "zphot_l95",
        "zphot_u95",
        "zspec",
        "zspec_err",
        "gx_type",
        "lumdist",
        "lumdist_err"
        "zbest",
        "zbest_err",
        "zbest_cmb",
        "zbest_err_cmb",
        ]
    
    glade_chunk = pd.read_csv(filename, chunksize=5000, names=columns, sep=r"\s+")
    return glade_chunk


def add_galaxies_sim(df, engine, table):
    
    rows = df.rename(
        columns={
            "glade": "id",
            "coord": "hpx",
            "redshift_cmb": "redshift",
            "zerror": "redshift_error",
            "m_B": "B_mag",
            "m_K": "K_mag",
            "m_W1": "W1_mag",
        }
    ).to_dict("records")

    with sa.orm.Session(engine) as session:
        session.bulk_insert_mappings(table, rows)
        session.commit()
    return


def create_fake_catalogs() -> None:

    engine = sa.create_engine("postgresql://localhost/gwcosmo_db")
    Base.metadata.create_all(engine)
    glade_chunk = read_in_catalog()

    for chunk in glade_chunk:
        chunk_filtered = glade_filter(chunk, radians=True)
        add_galaxies_sim(chunk_filtered, engine, GalaxySim)

    with sa.orm.Session(engine) as session:
        session.execute(sa.text("ANALYZE"))
