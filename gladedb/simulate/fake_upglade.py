import numpy as np
import pandas as pd
from astropy.coordinates import SkyCoord
from astropy import units as u
import warnings
warnings.filterwarnings("ignore", "Wswiglal-redir-stdio")
from bilby.gw.conversion import redshift_to_luminosity_distance
import sqlalchemy as sa  # type: ignore
from ..classes import Galaxy


from astropy.coordinates import ICRS
from astropy_healpix import level_to_nside, HEALPix
from mocpy import MOC # type: ignore

LEVEL = MOC.MAX_ORDER
HPX = HEALPix(nside=level_to_nside(LEVEL), order='nested', frame=ICRS())

# Number of sources in each catalog
# Testing with 1/16th of the sky
# With 2900000000/16 = 181,250,000 rows and 66 columns 
# at 8 bytes per float, 4 bytes per integer, this is ~90 GB
NUM_DR = 400000/16
NUM_WISE = 1900000000/16 
NUM_PANSTARRS = 2900000000/16
NUM_SDSS = 2600000/16 #2.6 million DR17, DR18 is out
NUM_SKYMAPPER = 8700000/16 # SELECT COUNT(class_star) from dr4.master where class_star <= 0.1; 8.7 milliion
NUM_GLADEPLUS = 21000000/16
NUM_UPGLADE = int(max(NUM_DR, NUM_WISE, NUM_PANSTARRS, NUM_SDSS, NUM_SKYMAPPER, NUM_GLADEPLUS))

FRAC_WISE = NUM_WISE / NUM_UPGLADE
FRAC_PANSTARRS = NUM_PANSTARRS / NUM_UPGLADE

def create_start_index():
    return np.random.randint(100,10000)

start_indices = {catalog: create_start_index() 
                 for catalog in ['upglade','dr10', 'dr9_sga', 'dr8', 'wise', 
                                 'ps_obj', 'ps_unique', 'sdss', 'skymapper', 
                                 'gladep']
                }

mag_ranges = {'g': (20.0, 24.0), 'r': (20.0, 24.0), 'i': (20.0, 24.0), 
              'z': (20.0, 24.0), 'y': (20.0, 24.0), 'W1': (20.0, 24.0),
              'W2': (20.0, 24.0), 'W3': (20.0, 24.0), 'W4': (20.0, 24.0)}

# Constants used to convert redshifts from heliocentric to CMB frame
v_cmb = 369  # km/s
c = 299792  # km/s
ra_cmb = 167.9  # RA of CMB dipole in degrees
dec_cmb = -7.22  # Dec of CMB dipole in degrees

def generate_ra_dec(num_sources, ra_min=180, ra_max=270, dec_min=-45, dec_max=0):
    ra = np.random.uniform(ra_min, ra_max, num_sources)
    dec = np.random.uniform(dec_min, dec_max, num_sources)
    return ra, dec


def generate_radec_errors(num_sources, sigma=3/3600):  # 3 arcseconds errors
    ra_error = np.random.normal(0, sigma, num_sources)
    dec_error = np.random.normal(0, sigma, num_sources)
    return ra_error, dec_error


def generate_magnitudes(num_sources, mag_ranges):
    magnitude = {band: np.random.uniform(low, high, num_sources) for band, (low, high) in mag_ranges.items()}
    magnitude_error = {band: np.random.uniform(0.01, 0.1, num_sources) for band in mag_ranges}
    return magnitude, magnitude_error


def generate_redshifts(num_sources, ra_values):
    zphot = np.zeros(num_sources)
    for i, ra in enumerate(ra_values):
        if ra < 225:
            zphot[i] = 0.5 + 0.5 * np.sin(np.radians(ra * 2))
        else:
            zphot[i] = 1.0 + 0.5 * np.sin(np.radians((ra - 225) * 2))
    return zphot

def generate_redshift_errors(num_sources, sigma=0.03):
    zphot_err = np.random.normal(0, sigma, num_sources)
    return zphot_err

def generate_redshift_distribution(zphot_median):
    zphot_l68 = zphot_median - 0.1
    zphot_u68 = zphot_median + 0.1
    zphot_l95 = zphot_median - 0.2
    zphot_u95 = zphot_median + 0.2
    return zphot_l68, zphot_u68, zphot_l95, zphot_u95


def generate_kcorr_values(num_sources, bands):
    kcorr = {band: np.random.uniform(-0.01, -0.03, num_sources) for band in bands}
    dkdz = {band: np.random.uniform(-1, -6, num_sources) for band in bands}
    return kcorr, dkdz


def convert_to_cmb_frame(z_helio, ra, dec):
    ra_rad = np.radians(ra)
    dec_rad = np.radians(dec)
    
    ra_cmb_rad = np.radians(ra_cmb)
    dec_cmb_rad = np.radians(dec_cmb)
    
    cos_theta = (np.sin(dec_rad) * np.sin(dec_cmb_rad) +
                 np.cos(dec_rad) * np.cos(dec_cmb_rad) * np.cos(ra_rad - ra_cmb_rad))
    
    z_cmb = z_helio + (v_cmb / c) * cos_theta
    return z_cmb

def remove_fraction_of_sources(data, bands, indices):
    for band in bands:
        data[band][indices] = np.nan
    return data

def create_random_indices(num_sources, starting_index):
    num_sources = int(num_sources)
    starting_index = int(starting_index)
    random_upglade_indices = set()
    while len(random_upglade_indices) < num_sources:
        random_upglade_indices.add(np.random.randint(1, NUM_UPGLADE + 1))
    random_upglade_indices = np.array(list(random_upglade_indices))
    return random_upglade_indices, np.arange(starting_index, starting_index + num_sources)

def create_fake_upglade(chunk, chunk_size=10000):
    """Create a fake UPGLADE catalog with the given number of sources in each catalog.
    Assume the PanSTARRS dataset is complete.
    """

    start = chunk * chunk_size
    end = min(start + chunk_size, NUM_UPGLADE)

    upglade_id = np.arange(start + 1, end + 1)
    ra, dec = generate_ra_dec(chunk_size, dec_min=-30)
    ra_error, dec_error = generate_radec_errors(chunk_size)
    hpxs = HPX.skycoord_to_healpix(SkyCoord(ra=ra*u.deg, dec=dec*u.deg))
    hpx = np.asarray([int(h) for h in hpxs])

    magnitudes, magnitude_errors = generate_magnitudes(chunk_size, mag_ranges)
    kcorrections, dkdzs = generate_kcorr_values(chunk_size, bands=mag_ranges.keys())
    ps_objid = np.arange(start_indices['ps_obj'] + start, start_indices['ps_obj'] + end)
    ps_unique_id = np.arange(start_indices['ps_unique']+ start, start_indices['ps_unique'] + end)
    wise_id = np.arange(start_indices['wise']+ start, start_indices['wise'] + end)
    num_to_keep = int(FRAC_WISE * chunk_size)
    drop_wise_indices = np.random.choice(chunk_size, chunk_size - num_to_keep, replace=False)

    # Not all bands will have valid entries across all catalogs.
    # To mimic this, don't population all (or drop subset of) sources in all magnitude bands
    magnitudes = remove_fraction_of_sources(magnitudes, ['W1', 'W2', 'W3', 'W4'], drop_wise_indices)
    magnitude_errors = remove_fraction_of_sources(magnitude_errors, ['W1', 'W2', 'W3', 'W4'], drop_wise_indices)
    kcorrections = remove_fraction_of_sources(kcorrections, ['W1', 'W2', 'W3', 'W4'], drop_wise_indices)
    dkdzs = remove_fraction_of_sources(dkdzs, ['W1', 'W2', 'W3', 'W4'], drop_wise_indices)

    zphot = generate_redshifts(chunk_size, ra)
    zphot_err = generate_redshift_errors(chunk_size, sigma=0.03)
    zspec = generate_redshifts(chunk_size, ra)
    zspec_err = generate_redshift_errors(chunk_size, sigma=0.01)

    # Mimic the real data by dropping some of the zspec values
    # Assume the zphot values are always present in the catalog
    # Drop 25% of the zspec values but keep all of the zphot values
    # Set to np.nan so that they'll be NULL in the database
    num_to_drop = int(0.25 * len(zspec))
    drop_indices = np.random.choice(len(zspec), num_to_drop, replace=False)
    zspec[drop_indices] = np.nan
    zspec_err[drop_indices] = np.nan

    zphot_median = zphot
    zphot_l68, zphot_u68, zphot_l95, zphot_u95 = generate_redshift_distribution(zphot_median)
    gx_type = np.random.choice(['GALAXY1', 'GALAXY2', 'GALAXY3'],chunk_size)
    lumdist = redshift_to_luminosity_distance(zphot)
    lumdist_err = redshift_to_luminosity_distance(zphot_err)
    zbest = np.where(np.isnan(zspec), zphot, zspec)
    zbest_err = np.where(np.isnan(zspec), zphot_err, zspec_err)
    zbest_cmb = convert_to_cmb_frame(zbest, ra, dec)
    zbest_err_cmb = zbest_err
    
    upglade_chunk = pd.DataFrame({
        'id': upglade_id,
        'hpx': hpx,
        'ps_objid': ps_objid,
        'ps_uniqueid': ps_unique_id,
        'wise': wise_id,
        'ra': ra,
        'dec': dec,
        'ra_err': ra_error,
        'dec_err': dec_error,
        'g': magnitudes['g'],
        'g_err': magnitude_errors['g'],
        'g_kcorr': kcorrections['g'],
        'g_dkdz': dkdzs['g'],
        'r': magnitudes['r'],
        'r_err': magnitude_errors['r'],
        'r_kcorr': kcorrections['r'],
        'r_dkdz': dkdzs['r'],
        'i': magnitudes['i'],
        'i_err': magnitude_errors['i'],
        'i_kcorr': kcorrections['i'],
        'i_dkdz': dkdzs['i'],
        'z': magnitudes['z'],
        'z_err': magnitude_errors['z'],
        'z_kcorr': kcorrections['z'],
        'z_dkdz': dkdzs['z'],
        'y': magnitudes['y'],
        'y_err': magnitude_errors['y'],
        'y_kcorr': kcorrections['y'],
        'y_dkdz': dkdzs['y'],
        'w1': magnitudes['W1'],
        'w1_err': magnitude_errors['W1'],
        'w1_kcorr': kcorrections['W1'],
        'w1_dkdz': dkdzs['W1'],
        'w2': magnitudes['W2'],
        'w2_err': magnitude_errors['W2'],
        'w2_kcorr': kcorrections['W2'],
        'w2_dkdz': dkdzs['W2'],
        'w3': magnitudes['W3'],
        'w3_err': magnitude_errors['W3'],
        'w3_kcorr': kcorrections['W3'],
        'w3_dkdz': dkdzs['W3'],
        'w4': magnitudes['W4'],
        'w4_err': magnitude_errors['W4'],
        'w4_kcorr': kcorrections['W4'],
        'w4_dkdz': dkdzs['W4'],
        'zphot': zphot,
        'zphot_err': zphot_err,
        'zphot_median': zphot_median,
        'zphot_l68': zphot_l68,
        'zphot_u68': zphot_u68,
        'zphot_l95': zphot_l95,
        'zphot_u95': zphot_u95,
        'zspec': zspec,
        'zspec_err': zspec_err,
        'gx_type': gx_type,
        'lumdist': lumdist,
        'lumdist_err': lumdist_err,
        'zbest': zbest,
        'zbest_err': zbest_err,
        'zbest_cmb': zbest_cmb,
        'zbest_err_cmb': zbest_err_cmb
    })
    
    # Set to None so that they're NULL in the database
    upglade_chunk.loc[drop_wise_indices, 'wise_id'] = None
    upglade_chunk = upglade_chunk.where(pd.notnull(upglade_chunk), None)
    return upglade_chunk


def add_galaxies(df, engine, table):

    rows = df.to_dict(orient="records")
    with sa.orm.Session(engine) as session:
        session.bulk_insert_mappings(table, rows)
        session.commit()
    return

def update_catalog(df, engine, table):
    update_rows = df.to_dict(orient='records')

    with sa.orm.Session(engine) as session:
        session.bulk_update_mappings(table, update_rows)
        session.commit()
    return


dev = False
if dev:
    NUM_UPGLADE = 2
    NUM_DR = 1
    NUM_WISE = 1 #* FRAC_WISE
    NUM_PANSTARRS = 2 #* FRAC_PANSTARRS
    NUM_SDSS = 1
    NUM_SKYMAPPER = 1
    NUM_GLADEPLUS = 1


def create_fake_upglade_catalog(engine, chunk_size=1000) -> None:

    num_chunks = int((NUM_UPGLADE + chunk_size - 1) // chunk_size)

    for chunk in range(num_chunks):
        upglade_chunk = create_fake_upglade(chunk, chunk_size=chunk_size)
        add_galaxies(upglade_chunk, engine, Galaxy)

    update_fake_upglade_catalog(engine)

    with sa.orm.Session(engine) as session:
        session.execute(sa.text("ANALYZE"))


def update_fake_upglade_catalog(engine) -> None:

    # Not all items in the table will have valid entries
    # To mimic this, don't populate all sources for all catalogs
    upglade_ids, dr10_ids = create_random_indices(NUM_DR, start_indices['dr10'])
    update_catalog(pd.DataFrame({'id': upglade_ids, 'dr10': dr10_ids}), engine, Galaxy)
    
    upglade_ids, dr9_sga_ids = create_random_indices(NUM_DR, start_indices['dr9_sga'])
    update_catalog(pd.DataFrame({'id': upglade_ids, 'dr9_sga': dr9_sga_ids}), engine, Galaxy)

    upglade_ids, dr8_ids = create_random_indices(NUM_DR, start_indices['dr8'])
    update_catalog(pd.DataFrame({'id': upglade_ids, 'dr8': dr8_ids}), engine, Galaxy)

    upglade_ids, sdss_bestobjids = create_random_indices(NUM_SDSS, start_indices['sdss'])
    update_catalog(pd.DataFrame({'id': upglade_ids, 'sdss_bestobjid': sdss_bestobjids}), engine, Galaxy)

    upglade_ids, skymapper_ids = create_random_indices(NUM_SKYMAPPER, start_indices['skymapper'])
    update_catalog(pd.DataFrame({'id': upglade_ids, 'skymapper': skymapper_ids}), engine, Galaxy)

    upglade_ids, gladep_ids = create_random_indices(NUM_GLADEPLUS, start_indices['gladep'])
    update_catalog(pd.DataFrame({'id': upglade_ids, 'gladep': gladep_ids}), engine, Galaxy)

    return



