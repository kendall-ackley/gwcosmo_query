import numpy as np
from ligo.skymap.io import read_sky_map
import h5py
import healpy as hp

from astropy.table import Table
from .classes import Skymap, SkymapTile
from .helpers import nest2uniq

import sqlalchemy as sa  # type: ignore
from sqlalchemy.exc import IntegrityError  # type: ignore


def add_skymap_to_database(skymap_file, moc=True, nest=True, db_id=None):

    if moc:
        skymap = read_sky_map(skymap_file, moc=moc)
        tiles = make_skymap_tiles(skymap, gaussian=True)
    else:
        skymap, _ = read_sky_map(skymap_file, moc=moc, nest=nest)
        tiles = make_skymap_tiles(skymap, gaussian=True)

    engine = sa.create_engine("postgresql://localhost/gwcosmo_db")
    with sa.orm.Session(engine) as session:
        if db_id is None:
            # Get the maximum id in the Skymap table
            max_id = session.query(sa.func.max(Skymap.id)).scalar()
            if max_id is None:
                max_id = 0
            db_id = max_id + 1
        while True:
            try:
                print("Inserting skymap with id:", db_id)
                session.add(Skymap(id=db_id, tiles=tiles))
                session.commit()
                break
            except IntegrityError as e:
                session.rollback()  # Rollback the session to a clean state
                # If it fails, get the maximum id in the Skymap table and add 1 to it
                max_id = session.query(sa.func.max(Skymap.id)).scalar()
                db_id = max_id + 1
                print(
                    "Skymap with id already exists in db. Trying again with db_id:",
                    db_id,
                )

        session.execute(sa.text("ANALYZE"))


def make_skymap_tiles(skymap_probability, moc=True, gaussian=False):

    if not moc:
        nside = hp.get_nside(skymap_probability)
        ipix = np.arange(hp.nside2npix(nside), dtype=np.uint64)
        uniq = nest2uniq(nside, ipix)
        skymap_probability_density = skymap_probability / np.sum(
            skymap_probability * hp.nside2pixarea(nside)
        )

        moc_data = Table(
            [uniq, skymap_probability_density], names=("UNIQ", "PROBDENSITY")
        )
    else:
        moc_data = skymap_probability

    if gaussian:
        # Reduce size of skymap for tile generation
        moc_data = moc_data[moc_data["PROBDENSITY"] > 1e-10]

    # Bug that pandas doesn't keep the type of the column through the apply
    # So doing it by list comprehension instead
    # TODO: Fix this
    tiles = [
        SkymapTile(hpx=int(row["UNIQ"]), probdensity=np.float64(row["PROBDENSITY"]))
        for row in moc_data
    ]

    return tiles
