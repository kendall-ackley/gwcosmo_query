from sqlalchemy import orm  # type: ignore
import sqlalchemy as sa  # type: ignore
import healpix_alchemy as ha  # type: ignore
from sqlalchemy import DDL, event  # type: ignore
from sqlalchemy.ext.declarative import declared_attr, as_declarative  # type: ignore


def gist_index(index_type, apply_to_hpx=True):
    """Class decorator to add a GiST or SP-GiST index to a table."""

    def decorator(cls):
        if apply_to_hpx:
            event.listen(
                cls.__table__,
                "after_create",
                DDL(
                    f"CREATE INDEX ix_{cls.__tablename__}_hpx ON {cls.__tablename__} USING {index_type} (hpx);"
                ),
            )
        event.listen(
            cls.__table__,
            "after_create",
            DDL(
                f"CREATE INDEX ix_{cls.__tablename__}_id_hpx ON {cls.__tablename__} USING gist (id gist_int4_ops, hpx);"
            ),
        )
        return cls

    return decorator


@as_declarative()
class Base:

    @declared_attr
    def __tablename__(cls):
        return cls.__name__.lower()

    def to_dict(self):
        """Serialize the model. Based on:
        https://github.com/absent1706/sqlalchemy-mixins/blob/master/sqlalchemy_mixins/serialize.py
        """
        return {field.name: getattr(self, field.name) for field in self.__table__.c}


@gist_index("gist", apply_to_hpx=False)
class Galaxy(Base):
    """
    Each row of the Galaxy table represents a point in a catalog
    """

    # BIGINT and DOUBLE PRECISION (Float) columns
    dr10 = sa.Column(sa.BigInteger, nullable=True)
    dr9_sga = sa.Column(sa.BigInteger, nullable=True)
    dr8 = sa.Column(sa.BigInteger, nullable=True)
    wise = sa.Column(sa.BigInteger, nullable=True)
    ps_objid = sa.Column(sa.BigInteger, nullable=True)
    ps_uniqueid = sa.Column(sa.BigInteger, nullable=True)
    sdss_bestobjid = sa.Column(sa.BigInteger, nullable=True)
    skymapper = sa.Column(sa.BigInteger, nullable=True)
    gladep = sa.Column(sa.BigInteger, nullable=True)
    ra = sa.Column(sa.Float, nullable=False)
    dec = sa.Column(sa.Float, nullable=False)
    ra_err = sa.Column(sa.Float, nullable=True)
    dec_err = sa.Column(sa.Float, nullable=True)
    g = sa.Column(sa.Float, nullable=True)
    g_err = sa.Column(sa.Float, nullable=True)
    g_kcorr = sa.Column(sa.Float, nullable=True)
    g_dkdz = sa.Column(sa.Float, nullable=True)
    r = sa.Column(sa.Float, nullable=True)
    r_err = sa.Column(sa.Float, nullable=True)
    r_kcorr = sa.Column(sa.Float, nullable=True)
    r_dkdz = sa.Column(sa.Float, nullable=True)
    i = sa.Column(sa.Float, nullable=True)
    i_err = sa.Column(sa.Float, nullable=True)
    i_kcorr = sa.Column(sa.Float, nullable=True)
    i_dkdz = sa.Column(sa.Float, nullable=True)
    z = sa.Column(sa.Float, nullable=True)
    z_err = sa.Column(sa.Float, nullable=True)
    z_kcorr = sa.Column(sa.Float, nullable=True)
    z_dkdz = sa.Column(sa.Float, nullable=True)
    y = sa.Column(sa.Float, nullable=True)
    y_err = sa.Column(sa.Float, nullable=True)
    y_kcorr = sa.Column(sa.Float, nullable=True)
    y_dkdz = sa.Column(sa.Float, nullable=True)
    w1 = sa.Column(sa.Float, nullable=True)
    w1_err = sa.Column(sa.Float, nullable=True)
    w1_kcorr = sa.Column(sa.Float, nullable=True)
    w1_dkdz = sa.Column(sa.Float, nullable=True)
    w2 = sa.Column(sa.Float, nullable=True)
    w2_err = sa.Column(sa.Float, nullable=True)
    w2_kcorr = sa.Column(sa.Float, nullable=True)
    w2_dkdz = sa.Column(sa.Float, nullable=True)
    w3 = sa.Column(sa.Float, nullable=True)
    w3_err = sa.Column(sa.Float, nullable=True)
    w3_kcorr = sa.Column(sa.Float, nullable=True)
    w3_dkdz = sa.Column(sa.Float, nullable=True)
    w4 = sa.Column(sa.Float, nullable=True)
    w4_err = sa.Column(sa.Float, nullable=True)
    w4_kcorr = sa.Column(sa.Float, nullable=True)
    w4_dkdz = sa.Column(sa.Float, nullable=True)
    zphot = sa.Column(sa.Float, nullable=True)
    zphot_err = sa.Column(sa.Float, nullable=True)
    zphot_median = sa.Column(sa.Float, nullable=True)
    zphot_l68 = sa.Column(sa.Float, nullable=True)
    zphot_u68 = sa.Column(sa.Float, nullable=True)
    zphot_l95 = sa.Column(sa.Float, nullable=True)
    zphot_u95 = sa.Column(sa.Float, nullable=True)
    zspec = sa.Column(sa.Float, nullable=True)
    zspec_err = sa.Column(sa.Float, nullable=True)
    lumdist = sa.Column(sa.Float, nullable=True)
    lumdist_err = sa.Column(sa.Float, nullable=True)
    zbest = sa.Column(sa.Float, nullable=True)
    zbest_err = sa.Column(sa.Float, nullable=True)
    zbest_cmb = sa.Column(sa.Float, nullable=True)
    zbest_err_cmb = sa.Column(sa.Float, nullable=True)
    # Custom datatype columns
    hpx = sa.Column(ha.Point, index=True, nullable=False)
    # INTEGER and BOOLEAN columns
    id = sa.Column(sa.Integer, primary_key=True)
    gx_type = sa.Column(sa.SmallInteger, nullable=True)

    # TODO:
    # gx_type = sa.Column(sa.String, nullable=True) or
    # gx_type = sa.Column(sa.Enum('GALAXY', 'QSO'), nullable=True)
    # gx_type = sa.Column(sa.Enum('G', 'Q', 'A', ..), nullable=True)


class GalaxyPartition(Base):
    """
    Each row of the Galaxy table represents a point in a catalog
    """

    __tablename__ = "galaxy_partition"
    __table_args__ = {"postgresql_partition_by": "RANGE (ra)"}

    @classmethod
    def __declare_last__(cls):
        """Creating and Checking partition by raw SQL"""
        for start_ra in range(0, 360, 10):
            end_ra = start_ra + 10
            ddl = DDL(
                f"""CREATE TABLE IF NOT EXISTS "galaxy_partition_{start_ra}_{end_ra}" PARTITION OF "galaxy_partition" FOR VALUES FROM ({start_ra}) TO ({end_ra});
                ALTER TABLE "galaxy_partition_{start_ra}_{end_ra}" ADD CONSTRAINT "galaxy_partition_{start_ra}_{end_ra}_check" CHECK (ra >= {start_ra} AND ra < {end_ra});"""
            )
            event.listen(cls.__table__, "after_create", ddl)
            print(f"Created partition: galaxy_partition_{start_ra}_{end_ra}")

    # BIGINT and DOUBLE PRECISION (Float) columns
    dr10 = sa.Column(sa.BigInteger, nullable=True)
    dr9_sga = sa.Column(sa.BigInteger, nullable=True)
    dr8 = sa.Column(sa.BigInteger, nullable=True)
    wise = sa.Column(sa.BigInteger, nullable=True)
    ps_objid = sa.Column(sa.BigInteger, nullable=True)
    ps_uniqueid = sa.Column(sa.BigInteger, nullable=True)
    sdss_bestobjid = sa.Column(sa.BigInteger, nullable=True)
    skymapper = sa.Column(sa.BigInteger, nullable=True)
    gladep = sa.Column(sa.BigInteger, nullable=True)
    ra = sa.Column(sa.Float, index=True, nullable=False)
    dec = sa.Column(sa.Float, nullable=False)
    ra_err = sa.Column(sa.Float, nullable=True)
    dec_err = sa.Column(sa.Float, nullable=True)
    g = sa.Column(sa.Float, nullable=True)
    g_err = sa.Column(sa.Float, nullable=True)
    g_kcorr = sa.Column(sa.Float, nullable=True)
    g_dkdz = sa.Column(sa.Float, nullable=True)
    r = sa.Column(sa.Float, nullable=True)
    r_err = sa.Column(sa.Float, nullable=True)
    r_kcorr = sa.Column(sa.Float, nullable=True)
    r_dkdz = sa.Column(sa.Float, nullable=True)
    i = sa.Column(sa.Float, nullable=True)
    i_err = sa.Column(sa.Float, nullable=True)
    i_kcorr = sa.Column(sa.Float, nullable=True)
    i_dkdz = sa.Column(sa.Float, nullable=True)
    z = sa.Column(sa.Float, nullable=True)
    z_err = sa.Column(sa.Float, nullable=True)
    z_kcorr = sa.Column(sa.Float, nullable=True)
    z_dkdz = sa.Column(sa.Float, nullable=True)
    y = sa.Column(sa.Float, nullable=True)
    y_err = sa.Column(sa.Float, nullable=True)
    y_kcorr = sa.Column(sa.Float, nullable=True)
    y_dkdz = sa.Column(sa.Float, nullable=True)
    w1 = sa.Column(sa.Float, nullable=True)
    w1_err = sa.Column(sa.Float, nullable=True)
    w1_kcorr = sa.Column(sa.Float, nullable=True)
    w1_dkdz = sa.Column(sa.Float, nullable=True)
    w2 = sa.Column(sa.Float, nullable=True)
    w2_err = sa.Column(sa.Float, nullable=True)
    w2_kcorr = sa.Column(sa.Float, nullable=True)
    w2_dkdz = sa.Column(sa.Float, nullable=True)
    w3 = sa.Column(sa.Float, nullable=True)
    w3_err = sa.Column(sa.Float, nullable=True)
    w3_kcorr = sa.Column(sa.Float, nullable=True)
    w3_dkdz = sa.Column(sa.Float, nullable=True)
    w4 = sa.Column(sa.Float, nullable=True)
    w4_err = sa.Column(sa.Float, nullable=True)
    w4_kcorr = sa.Column(sa.Float, nullable=True)
    w4_dkdz = sa.Column(sa.Float, nullable=True)
    zphot = sa.Column(sa.Float, nullable=True)
    zphot_err = sa.Column(sa.Float, nullable=True)
    zphot_median = sa.Column(sa.Float, nullable=True)
    zphot_l68 = sa.Column(sa.Float, nullable=True)
    zphot_u68 = sa.Column(sa.Float, nullable=True)
    zphot_l95 = sa.Column(sa.Float, nullable=True)
    zphot_u95 = sa.Column(sa.Float, nullable=True)
    zspec = sa.Column(sa.Float, nullable=True)
    zspec_err = sa.Column(sa.Float, nullable=True)
    lumdist = sa.Column(sa.Float, nullable=True)
    lumdist_err = sa.Column(sa.Float, nullable=True)
    zbest = sa.Column(sa.Float, nullable=True)
    zbest_err = sa.Column(sa.Float, nullable=True)
    zbest_cmb = sa.Column(sa.Float, nullable=True)
    zbest_err_cmb = sa.Column(sa.Float, nullable=True)
    # Custom datatype columns
    hpx = sa.Column(ha.Point, index=True, nullable=False)
    # INTEGER and BOOLEAN columns
    id = sa.Column(sa.Integer, primary_key=True)
    gx_type = sa.Column(sa.SmallInteger, nullable=True)


class Skymap(Base):
    """
    Each row of the Skymap table represents a LIGO/Virgo
    HEALPix localization map.
    """

    id = sa.Column(sa.Integer, primary_key=True)
    tiles = orm.relationship(lambda: SkymapTile)


@gist_index("spgist", apply_to_hpx=True)
class SkymapTile(Base):
    """
    Each row of the SkymapTile table represents a multi-resolution
    HEALPix tile within a LIGO/Virgo localization map. There is a
    one-to-many mapping between Skymap and SkymapTile.
    """

    # BIGINT and DOUBLE PRECISION (Float) columns
    probdensity = sa.Column(sa.Float, nullable=False)
    # Custom datatype columns
    hpx = sa.Column(ha.Tile, primary_key=True)
    # INTEGER and BOOLEAN columns
    id = sa.Column(sa.ForeignKey(Skymap.id), primary_key=True)


@gist_index("gist", apply_to_hpx=False)
class GalaxySim(Base):
    """
    Each row of the GalaxySim table represents a point in a catalog
    This is for testing purposes only
    """

    # BIGINT and DOUBLE PRECISION (Float) columns
    redshift = sa.Column(sa.Float, index=True, nullable=True)
    redshift_error = sa.Column(sa.Float, index=True, nullable=True)
    B_mag = sa.Column(sa.Float, index=True, nullable=True)
    K_mag = sa.Column(sa.Float, index=True, nullable=True)
    W1_mag = sa.Column(sa.Float, index=True, nullable=True)
    ra = sa.Column(sa.Float, nullable=False)
    dec = sa.Column(sa.Float, nullable=False)
    # Custom datatype columns
    hpx = sa.Column(ha.Point, index=True, nullable=False)
    # INTEGER and BOOLEAN columns
    id = sa.Column(sa.Integer, primary_key=True)


class SkymapSim(Base):
    """
    Each row of the Skymap table represents a LIGO/Virgo
    HEALPix localization map.
    """

    id = sa.Column(sa.Integer, primary_key=True)
    tiles = orm.relationship(lambda: SkymapTileSim)


@gist_index("spgist", apply_to_hpx=True)
class SkymapTileSim(Base):
    """
    Each row of the SkymapTile table represents a multi-resolution
    HEALPix tile within a LIGO/Virgo localization map. There is a
    one-to-many mapping between Skymap and SkymapTile.
    """

    # BIGINT and DOUBLE PRECISION (Float) columns
    probdensity = sa.Column(sa.Float, nullable=False)
    # Custom datatype columns
    hpx = sa.Column(ha.Tile, primary_key=True)
    # INTEGER and BOOLEAN columns
    id = sa.Column(sa.ForeignKey(SkymapSim.id), primary_key=True)
