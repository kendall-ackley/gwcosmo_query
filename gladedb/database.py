import numpy as np
import astropy.units as u
from sqlalchemy.ext.hybrid import hybrid_property  # type: ignore


@hybrid_property
def q3c_radial_query(ra1, dec1, ra2, dec2):
    """
    Calculate the spherical distance between two points on the celestial sphere.
    Work around for q3c install on AMR64 architecture
    https://github.com/segasai/q3c

    Args:
    ra1, dec1, ra2, dec2 : float
        Right ascension and declination of the two points (in degrees).

    Returns:
    distance : float
        The spherical distance (in degrees).
    """

    ra1, dec1, ra2, dec2 = np.radians([ra1, dec1, ra2, dec2])
    delta_ra = ra2 - ra1
    delta_dec = dec2 - dec1
    a = (
        np.sin(delta_dec / 2) ** 2
        + np.cos(dec1) * np.cos(dec2) * np.sin(delta_ra / 2) ** 2
    )
    distance = 2 * np.arcsin(np.sqrt(a))
    distance = (np.degrees(distance) * u.deg).to(u.arcsec).value

    return distance
