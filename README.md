

# Repository Name

This repository contains tools for querying GW cosmology data.

## Prerequisites

Before you can use this repository, you need to have the following installed:

- [Poetry](https://python-poetry.org/): A dependency management and packaging tool for Python.
- [PostgreSQL 14+](https://www.postgresql.org/): A powerful, open-source object-relational database system.
- [Q3C](https://github.com/segasai/q3c): PostgreSQL extension for spatial indexing on a sphere.

Note: If using ARM64 architecture (MacOS M1/M2 chips), the [Q3C extension](https://github.com/segasai/q3c) *cannot* be applied to the database. Work-arounds are planned until future versions of PostgreSQL are developed to include more complete support for ARM64.

# Install PostgreSQL and create the database

## For local installation
Installation tutorial: https://www.postgresql.org/docs/current/tutorial-install.html

On MacOS:
```
brew install postgresql@15
brew services start postgresql@15
createdb gwcosmo_db
```

## For connecting to Docker
Install [Docker](https://docs.docker.com/get-docker/). Once this is downloaded you'll need to open the app to start the instance and pull over the `postgresql` docker image.
```
docker pull postgres
```

Then using the `docker-compose.yml` file located in this directory run on the command line:
```
docker-compose -f docker-compose.yml up -d
psql -U postgres -d gwcosmo_db -h localhost -W
```
You should then be connected to the database from outside of the container.

# Add gist extension to database
```
psql -d gwcosmo_db -c "CREATE EXTENSION IF NOT EXISTS btree_gist;"
```

# Add Q3C extension to database
*not ARM64-compatible
```
psql -d gwcosmo_db -c "CREATE EXTENSION IF NOT EXISTS q3c;"
```

# Catalogs
- [GLADE+ Catalog](http://elysium.elte.hu/~dalyag/GLADE+.txt)


# Example
See `RunScripts.ipynb` for tutorials on how to interact with the database through python.

Notes: 
- This example to set up and make use of a PostgreSQL database are meant for local installation. The databases cannot be created on the LIGO Data Grid but will instead be containerized.
